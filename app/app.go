package app
import (
	"fmt"
	"net/http"
	"github.com/gorilla/mux"
)

type App struct {
	Name string
}

func (a *App ) RegisterRouters(r *mux.Router) {
	fmt.Println("registerRouters")
	fmt.Printf("Name %s\n", a.Name)
	a.registerRootRouter(r)
}

func (a *App) registerRootRouter(r *mux.Router) {
	apiRouter := r.PathPrefix("/").Subrouter()
	apiRouter.Methods("GET").Path("/").HandlerFunc(mainRouter)
}

func mainRouter(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("calling main router"))
}