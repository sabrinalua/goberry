package main 

import (
	"os"
	"log"
	"fmt"
	"sync"
	"time"
	 // "os/signal"
	// "syscall"
	"net/http"
	"io/ioutil"
	"encoding/json"
	// "gitlab.com/sabrinalua/s3"
	"gitlab.com/sabrinalua/goberry/app"
	"github.com/gorilla/mux"
	"github.com/gorilla/handlers"
)

var (
  application *app.App
  configLock = new(sync.RWMutex)
)

func main(){
	fmt.Println("go-app main", application )
	router := mux.NewRouter().SkipClean(true)
	logFile, _ := os.OpenFile("server.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	routeLogger:= handlers.CombinedLoggingHandler(logFile, router)
	application.RegisterRouters(router)

	srv:= &http.Server{
		Addr: "0.0.0.0:1080",
		Handler: routeLogger,
		WriteTimeout: 15*time.Second,
		ReadTimeout: 15*time.Second,
	}
	fmt.Println("listening on port 1080")
	srv.ListenAndServe()
}

func init() {
	loadConfig(true)
	  // s := make(chan os.Signal, 1)
	  // signal.Notify(s, syscall.SIGUSR2)
	  // go func() {
	  //   for {
	  //     <-s
	  //     loadConfig(false)
	  //     log.Println("Reloaded")
	  //   }
	  // }()
}

func loadConfig(fail bool){
  file, err := ioutil.ReadFile("config.json")
  if err != nil {
    log.Println("open config: ", err)
    if fail { os.Exit(1) }
  }

  temp := new(app.App)
  if err = json.Unmarshal(file, temp); err != nil {
    log.Println("parse config: ", err)
    if fail { os.Exit(1) }
  }
  configLock.Lock()
  application = temp
  configLock.Unlock()
}

